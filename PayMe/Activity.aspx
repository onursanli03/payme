﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Activity.aspx.cs" Inherits="PayMe.Activity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-12 col-md-offset-2">
        <div class="form form-inline">
            <input runat="server" class="form-control" type="text" id="birthDate1" name="birthDate1" placeholder="dd-MM-yyyy" />
            <script type="text/javascript">
                $(document).ready(function () {
                    var dp = $('#<%=birthDate1.ClientID%>');
                    dp.datepicker({
                        changeMonth: true,
                        changeYear: true,
                        format: "dd-mm-yyyy",
                        language: "tr",
                        autoclose: true,
                        todayHighlight: true
                    }).on('changeDate', function (ev) {
                        $(this).blur();
                        $(this).datepicker('hide');
                    });
                });
            </script>

            <input runat="server" class="form-control" type="text" id="birthDate2" name="birthDate2" placeholder="dd-MM-yyyy">
            <script type="text/javascript">
                $(document).ready(function () {
                    var dp = $('#<%=birthDate2.ClientID%>');
                    dp.datepicker({
                        changeMonth: true,
                        changeYear: true,
                        format: "dd-mm-yyyy",
                        language: "tr",
                        autoclose: true,
                        todayHighlight: true
                    }).on('changeDate', function (ev) {
                        $(this).blur();
                        $(this).datepicker('hide');
                    });
                });
            </script>

            <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Height="29px" OnClick="Button1_Click" Style="margin-top: 0" Text="Search By Date" />
        </div>
        <br />
        <div class="form form-inline">
            <asp:DropDownList ID="TypeDropList" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="TypeDropList_SelectedIndexChanged">
                <asp:ListItem>SENDED</asp:ListItem>
                <asp:ListItem>RECEIVED</asp:ListItem>
            </asp:DropDownList>
        </div>
        <br />
        <div class="form form-inline">
            <asp:TextBox ID="getNameTxt" CssClass="form-control" runat="server" placeholder="Enter Name" AutoPostBack="True" OnTextChanged="getNameTxt_TextChanged"></asp:TextBox>
            <asp:Button ID="Button3" runat="server" CssClass="btn btn-success" OnClick="Button3_Click" Text="Search By Name" />
            <asp:Label ID="NameWarning" runat="server" Text="Label" Visible="False" CssClass="lbl label-warning"></asp:Label>
        </div>

        <br />

        <div class="col-md-8">

            <asp:DataList ID="ActivityList" runat="server">
                <HeaderTemplate>
                    <table style="width: 100%;" class="table table-bordered">
                        <thead class="thead-inverse">
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Transaction Type</th>
                                <th>Transaction Time</th>
                                <th>Transaction Amount</th>
                                <th>Select Invoice</th>
                            </tr>
                        </thead>
                    </table>
                </HeaderTemplate>

                <ItemTemplate>
                    <table style="width: 100%;" class="table table-responsive">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Label ID="Fnamelbl" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Snamelbl" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="TransTypelbl" runat="server" Text='<%# Eval("DoneTransactionType") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="TransTimelbl" runat="server" Text='<%# Eval("TransactionTime") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="TransAmounlbl" runat="server" Text='<%# Eval("TransactionAmount") %>'></asp:Label>

                                    <asp:Label ID="SenderCurTypes" runat="server" Text='<%# Eval("CurrencyType") %>'></asp:Label>
                                </td>
                                <td>
                                    <td>
                                        <input id="Invoice" type="radio" name="InvoiceGroup" value='<%#Eval("DoneTransactionTypeID") %>' />
                                    </td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </ItemTemplate>
            </asp:DataList>
            <asp:Button ID="Button4" CssClass="btn btn-primary" runat="server" OnClick="Button4_Click" Text="Create Invoice" />
        </div>
    </div>

</asp:Content>
