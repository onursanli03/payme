﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMe
{
    public partial class Activity : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string UserID = (string)Session["UserID"];


                DataTable dt = ShowActivityByUserID(Int16.Parse(UserID));
                if (dt.Rows.Count > 0)
                {

                    ActivityList.DataSource = dt;
                    ActivityList.DataBind();



                }

            }


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string UserID = (string)Session["UserID"];


            DateTime date1 = DateTime.ParseExact(birthDate1.Value, "MM-dd-yyyy", CultureInfo.InvariantCulture);
            DateTime date2 = DateTime.ParseExact(birthDate2.Value, "MM-dd-yyyy", CultureInfo.InvariantCulture);

            DataTable dt = ShowActivityBtwDateByID(Int16.Parse(UserID), date1, date2);
            if (dt.Rows.Count > 0)
            {

                ActivityList.DataSource = dt;
                ActivityList.DataBind();



            }


        }



        protected void Button3_Click(object sender, EventArgs e)
        {

            string UserID = (string)Session["UserID"];

            string Name = " ";
            string SurName = " ";


            string text = getNameTxt.Text;
            // string []array= Regex.Split(text, "");
            string[] array = text.Split(' ');//divide textbox content to anme,surname

            if (array.Length == 0)
            {
                Name = " ";
                SurName = " ";


            }
            else if (array.Length == 1)
            {
                Name = array[0].ToString();

            }
            else if (array.Length == 2)
            {
                Name = array[0].ToString();
                SurName = array[1].ToString();
            }

            DataTable dt = ShowActivityByUserName(Int16.Parse(UserID), Name, SurName);

            if (dt.Rows.Count > 0)
            {


                ActivityList.DataSource = dt;
                ActivityList.DataBind();

            }
            else
            {
                NameWarning.Visible = true;
                NameWarning.Text = "Name could not found ";

            }

        }


        public DataTable ShowActivityBtwDateByID(int UserID, DateTime date1, DateTime date2)
        {



            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.ShowActivityBtwDateByID";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);

            com.Parameters.AddWithValue("@StartDate", date1);
            com.Parameters.AddWithValue("@FinishDate", date2);
            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;


        }



        public DataTable ShowActivityByUserID(int UserID)
        {



            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.ShowActivityByUserID";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);
            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;


        }


        public DataTable ShowActivityByTransType(int UserID, string TransactionType)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;


            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.ShowActivityByTransType";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);
            com.Parameters.AddWithValue("@TransactionType", TransactionType);


            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;


        }

        public DataTable ShowActivityByUserName(int UserID, string UserName, string UserSurname)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;


            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.ShowActivityByUserName";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);
            com.Parameters.AddWithValue("@UserName", UserName);
            com.Parameters.AddWithValue("@UserSurname", UserSurname);


            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;


        }

        protected void TypeDropList_SelectedIndexChanged(object sender, EventArgs e)
        {

            string UserID = (string)Session["UserID"];



            DataTable dt = ShowActivityByTransType(Int16.Parse(UserID), TypeDropList.SelectedValue);
            if (dt.Rows.Count > 0)
            {

                ActivityList.DataSource = dt;
                ActivityList.DataBind();



            }

        }

        protected void Button4_Click(object sender, EventArgs e)
        {


            if (Request.Form["InvoiceGroup"] != null)

            {

                Session["DoneTransID"] = Request.Form["InvoiceGroup"];
                Response.Redirect("Invoice.aspx");
            }
        }

        protected void getNameTxt_TextChanged(object sender, EventArgs e)
        {

            string UserID = (string)Session["UserID"];

            string Name = " ";
            string SurName = " ";


            string text = getNameTxt.Text;
            // string []array= Regex.Split(text, "");
            string[] array = text.Split(' ');//divide textbox content to anme,surname

            if (array.Length == 0)
            {
                Name = " ";
                SurName = " ";


            }
            else if (array.Length == 1)
            {
                Name = array[0].ToString();

            }
            else if (array.Length == 2)
            {
                Name = array[0].ToString();
                SurName = array[1].ToString();
            }

            DataTable dt = ShowActivityByUserName(Int16.Parse(UserID), Name, SurName);

            if (dt.Rows.Count > 0)
            {


                ActivityList.DataSource = dt;
                ActivityList.DataBind();

            }
            else
            {

                NameWarning.Visible = true;
                NameWarning.Text = "Name could not found "; ;
            }


        }
    }
}