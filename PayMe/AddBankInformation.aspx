﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddBankInformation.aspx.cs" Inherits="PayMe.AddBankInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">



    <div class="container">

        <div class="col-md-10">
            <div class="col-md-6">
                <h3 style="text-align: center">Add Bank Account</h3>
                <hr />
            </div>
            <div class="col-md-6 col-md-offset-4">
                <asp:DropDownList CssClass="form-control" ID="bankList" runat="server">
                    <asp:ListItem Value="AB">Akbank</asp:ListItem>
                    <asp:ListItem Value="IB">İş Bankası</asp:ListItem>
                    <asp:ListItem Value="ZB">Ziraat Bankası</asp:ListItem>
                    <asp:ListItem Value="GB">Garanti Bankası</asp:ListItem>
                    <asp:ListItem Value="TEB">Türkiye Ekonomi Bankası</asp:ListItem>
                    <asp:ListItem Value="DB">Deniz Bank</asp:ListItem>
                </asp:DropDownList>
                <br />
                <asp:TextBox ID="txtAccountNumber" CssClass="form-control" runat="server"></asp:TextBox>
                <br />
                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-success btn-block" Text="Save" placeholder="Account Number" OnClick="btnSave_Click" />
            </div>
        </div>
    </div>





</asp:Content>
