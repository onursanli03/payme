﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddCard.aspx.cs" Inherits="PayMe.AddCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">



    <div class="container">
        <div class="col-md-10">
            <div class="co-md-6">
                <h3 style="text-align: center">Add Credit Card</h3>
                <hr />
            </div>

            
            <div class="co-md-6 col-md-offset-4">

                <asp:TextBox ID="txtCardNumber" CssClass="form-control" runat="server" placeholder="Card Number"></asp:TextBox>
                <br />
                <asp:TextBox ID="txtExpireDate" CssClass="form-control" runat="server" placeholder="Expire Date"></asp:TextBox>
                <br />
                <asp:TextBox ID="txtCVV" runat="server" CssClass="form-control" placeholder="CVV"></asp:TextBox>
                <br />
                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-success btn-block" Text="Save" OnClick="btnSave_Click" />
            </div>
        </div>
    </div>





</asp:Content>
