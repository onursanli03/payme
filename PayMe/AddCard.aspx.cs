﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMe
{
    public partial class AddCard : System.Web.UI.Page
    {

        string conStr = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
           
            SqlConnection con = new SqlConnection(conStr);
            string query = "Info.SaveCardInformation";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", Session["UserID"]);
            com.Parameters.AddWithValue("@cardNumber", txtCardNumber.Text);
            com.Parameters.AddWithValue("@expireDate", txtExpireDate.Text);
            com.Parameters.AddWithValue("@cvv", Int16.Parse( txtCVV.Text));
           
            com.CommandType = System.Data.CommandType.StoredProcedure;
            con.Open();
            com.ExecuteNonQuery();
            con.Close();
           

            Response.Redirect("summary.aspx");
        }
    }
}