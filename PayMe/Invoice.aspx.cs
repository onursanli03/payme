﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace PayMe
{
    public partial class Invoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                string DoneTransID = (string)Session["DoneTransID"];
                string InvoiceControlName = RandomString(15);
                string UserID = (string)Session["UserID"];
                int result;
                result = CreateInvoice(Int16.Parse(UserID), Int16.Parse(DoneTransID), InvoiceControlName);
                //finish create invoice

                if (result > 0)
                {
                    
                    DataTable dt3 = DisplayInvoiceByControl(InvoiceControlName);

                    SenderFname.Text = dt3.Rows[0]["SenderFirstName"].ToString();
                    SenderSname.Text = dt3.Rows[0]["SenderLastName"].ToString();
                    SenderSSN.Text = dt3.Rows[0]["SenderSSN"].ToString();
                    SenderDnTrType.Text = dt3.Rows[0]["DoneTransactionType"].ToString();
                    SenderTrTime.Text = dt3.Rows[0]["TransactionTime"].ToString();
                    SenderTrAmount.Text = dt3.Rows[0]["TransactionAmount"].ToString();
                    ReceiverFname.Text = dt3.Rows[0]["ReceiverFirstName"].ToString();
                    ReceiverSname.Text = dt3.Rows[0]["ReceiverLastName"].ToString();
                    ReceiverSSN.Text = dt3.Rows[0]["ReceiverSSN"].ToString();
                    InvoiceControlID.Text = dt3.Rows[0]["InvoiceControl"].ToString();
                    SenderCurType.Text = dt3.Rows[0]["CurrencyType"].ToString();
                    

                   

                }
            }


        }

        public int CreateInvoice(int UserID, int DoneTransactionTypeID, string InvoiceControlName)
        {



            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);

            string query = "Info.CreateInvoice";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);

            com.Parameters.AddWithValue("@DoneTransactionTypeID", DoneTransactionTypeID);
            com.Parameters.AddWithValue("@InvoiceControl", InvoiceControlName);

            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Open();
            int res = com.ExecuteNonQuery();
            con.Close();


            return res;
        }



        public DataTable DisplayInvoiceByControl(string InvoiceControlName)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;
            SqlConnection con2 = new SqlConnection(connectionString);

            string query2 = "Info.DisplayInvoiceByControl";
            SqlCommand com2 = new SqlCommand(query2, con2);
            con2.Open();

            com2.Parameters.AddWithValue("@InvoiceControl", InvoiceControlName);

            com2.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da2 = new SqlDataAdapter(com2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);

            return dt2;
        }
        private string RandomString(int Size)
        {
            string input = "abcdefghijklmnopqrstuvwxyz0123456789";
            StringBuilder builder = new StringBuilder();
            char ch;
            Random random = new Random();
            for (int i = 0; i < Size; i++)
            {
                ch = input[random.Next(0, input.Length)];
                builder.Append(ch);
            }
            return builder.ToString();
        }

    }
}