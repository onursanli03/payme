﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MyMaster.Master" AutoEventWireup="true" CodeBehind="InvoiceControl.aspx.cs" Inherits="PayMe.InvoiceControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
    <div class="form form-inline">
    <strong>
    <asp:Label ID="Label1" CssClass="label label-default" runat="server" Text="Invoice Key"></asp:Label>
    </strong>
    <asp:TextBox ID="controlInvoicetxt" CssClass="form-control" runat="server"></asp:TextBox>
    <asp:Button ID="Button1" runat="server" CssClass="btn btn-success" OnClick="Button1_Click" Text="control invoice" />
    </div>
    <div class="col-md-12 col-md-offset-2">

        <div class="col-md-8 col-md-offset-2">
            <img src="image/Logo_payme_0_120.png" />
        </div>
        <br />
        <hr />
        <div class="table table-responsive">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <strong>
                            <asp:Label ID="label2" runat="server" Text="Sender Name : "></asp:Label>
                        </strong>
                    </td>
                    <td>
                        <asp:Label ID="SenderFname" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <asp:Label ID="label3" runat="server" Text="Sender Surname : "></asp:Label>
                        </strong>
                    </td>
                    <td>
                        <asp:Label ID="SenderSname" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <asp:Label ID="label4" runat="server" Text="Sender SSN : "></asp:Label>
                        </strong>
                    </td>
                    <td>
                        <asp:Label ID="SenderSSN" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <asp:Label ID="label5" runat="server" Text="Sender Transaction Type : "></asp:Label>
                        </strong>
                    </td>
                    <td>
                        <asp:Label ID="SenderDnTrType" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <asp:Label ID="label6" runat="server" Text="Sender Transaction Time : "></asp:Label>
                        </strong>
                    </td>
                    <td>
                        <asp:Label ID="SenderTrTime" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>

                    <td>
                        <strong>
                            <asp:Label ID="label7" runat="server" Text="Sender Amount : "></asp:Label>
                        </strong>
                    </td>
                    <td>
                        <asp:Label ID="SenderTrAmount" runat="server" Text="Label"></asp:Label>
                        <asp:Label ID="SenderCurType" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <asp:Label ID="label8" runat="server" Text="Reciever Name : "></asp:Label>
                        </strong>
                    </td>
                    <td>
                        <asp:Label ID="ReceiverFname" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <asp:Label ID="label9" runat="server" Text="Reciever Surname : "></asp:Label>
                        </strong>
                    </td>
                    <td>
                        <asp:Label ID="ReceiverSname" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <asp:Label ID="label10" runat="server" Text="Reciever SSN : "></asp:Label>
                        </strong>
                    </td>
                    <td>
                        <asp:Label ID="ReceiverSSN" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <asp:Label ID="label11" runat="server" Text="Invoice Control Key : "></asp:Label>
                        </strong>
                    </td>
                    <td>
                        <asp:Label ID="InvoiceControlID" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>

            </table>
            <br />
            
        </div>
    </div>
</asp:Content>
