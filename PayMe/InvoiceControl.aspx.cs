﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMe
{
    public partial class InvoiceControl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            DataTable datatbl = DisplayInvoiceByControl(controlInvoicetxt.Text);

            SenderFname.Text = datatbl.Rows[0]["SenderFirstName"].ToString();
            SenderSname.Text = datatbl.Rows[0]["SenderLastName"].ToString();
            SenderSSN.Text = datatbl.Rows[0]["SenderSSN"].ToString();
            SenderDnTrType.Text = datatbl.Rows[0]["DoneTransactionType"].ToString();
            SenderTrTime.Text = datatbl.Rows[0]["TransactionTime"].ToString();
            SenderTrAmount.Text = datatbl.Rows[0]["TransactionAmount"].ToString();
            SenderCurType.Text = datatbl.Rows[0]["CurrencyType"].ToString();
            ReceiverFname.Text = datatbl.Rows[0]["ReceiverFirstName"].ToString();
            ReceiverSname.Text = datatbl.Rows[0]["ReceiverLastName"].ToString();
            ReceiverSSN.Text = datatbl.Rows[0]["ReceiverSSN"].ToString();
            InvoiceControlID.Text = datatbl.Rows[0]["InvoiceControl"].ToString();


        }


        public DataTable DisplayInvoiceByControl(string InvoiceControlName)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;
            SqlConnection con2 = new SqlConnection(connectionString);

            string query2 = "Info.DisplayInvoiceByControl";
            SqlCommand com2 = new SqlCommand(query2, con2);
            con2.Open();

            com2.Parameters.AddWithValue("@InvoiceControl", InvoiceControlName);

            com2.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da2 = new SqlDataAdapter(com2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);

            return dt2;
        }
    }
}
