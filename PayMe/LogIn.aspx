﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/MyMaster.Master" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="PayMe.LogIn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="container">
        <div class="row text-center " style="padding-top: 100px;">
            <div class="col-md-12">
                <img src="image/Logo_payme_0_500.png" />
            </div>
        </div>
        <div class="row ">

            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">

                <div class="panel-body">
                    <div role="form">
                        <hr />
                        <h5>Enter Details to Login</h5>
                        <br />
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                            <asp:TextBox ID="txtUserName" runat="server" placeholder="Your Username " CssClass="form-control"
                                type="text"></asp:TextBox>
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <asp:TextBox ID="txtPassword" runat="server" placeholder="Your Password " CssClass="form-control"
                                type="text" TextMode="Password"></asp:TextBox>
                        </div>
                         <div class="form-group">
                            <span class="pull-right">
                                <a href="index.html">Forget password ? </a>
                            </span>
                            <label class="checkbox-inline">
                                <asp:CheckBox ID="loginCheck" runat="server" Text="Remember Me" />
                             <asp:Label ID="caphe" runat="server" Text="Label"></asp:Label>
                            </label>
                             <asp:TextBox ID="caphetxt" runat="server"></asp:TextBox>
                        </div>
                        <asp:Button ID="loginButton" runat="server" Text="Login Now" CssClass="btn btn-primary" onclick="btnLogIn_Click"/>
                        <hr />
                        Not register ? <a href="SignUp.aspx">click here </a>or go to <a href="index.aspx">Home</a>
                    </div>
                </div>

            </div>
        </div>
    </div>









</asp:Content>
