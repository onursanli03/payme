﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMe
{
    public partial class LogIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            caphe.Text = "1234";
            if (!IsPostBack)
            {
                if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
                {
                    loginCheck.Checked = true;
                    txtUserName.Text = Request.Cookies["UserName"].Value;
                    txtPassword.Attributes["value"] = Request.Cookies["Password"].Value;
                }


            }
        }
        protected void btnSignUp_Click(object sender, EventArgs e)
        {

            Response.Redirect("SignUp.aspx");

        }

        protected void btnLogIn_Click(object sender, EventArgs e)
        {
            DataTable dt;
            dt = Login(txtUserName.Text, txtPassword.Text);



            if (dt.Rows.Count > 0)
            {

                if ((bool)dt.Rows[0]["isActive"] == true)
                {
                    Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                    Session["FirstName"] = dt.Rows[0]["FirstName"].ToString();
                    Session["LastName"] = dt.Rows[0]["LastName"].ToString();
                    Session["PicturePath"] = dt.Rows[0]["PicturePath"].ToString();

                    Session["accountType"] = dt.Rows[0]["AccountLimit"].ToString();
                    Response.Redirect("Summary.aspx");
                }

                else
                {
                    Response.Write("<script>alert('Account has deleted or suspended. If there is problem please contact us')</script>");

                }

            }
            else
            {
                Response.Write("<script>alert('Please enter valid Username and Password')</script>");

            }



        }
        public DataTable Login(string mail, string password)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from [Info].[Users] where ([Email]=@Email) AND [Password]=@Password", con);
            cmd.Parameters.AddWithValue("@Email", mail);
            cmd.Parameters.AddWithValue("@Password", password);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;

        }
        public void cookiefunc()
        {
            if (loginCheck.Checked)

            {
                Response.Cookies["UserName"].Expires = DateTime.Now.AddMinutes(1);
                Response.Cookies["Password"].Expires = DateTime.Now.AddMinutes(1);
            }
            else
            {
                Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);
            }
            Response.Cookies["UserName"].Value = txtUserName.Text.Trim();
            Response.Cookies["Password"].Value = txtPassword.Text.Trim();

        }


    }
}