﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SendAndRequest.aspx.cs" Inherits="PayMe.SendAndRequest" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">



    <div class="col-md-10">
        
        <h2 style="text-align:center"><img src="image/Logo_payme_2_50.png" /> Send Money</h2>
        
        <hr />
        <div class="col-md-offset-4 col-md-8">
        <strong>
            <asp:Label ID="Label1" runat="server" Text="Please enter mail"></asp:Label></strong>
        <asp:TextBox ID="txtSendMail" runat="server" CssClass="form-control" OnTextChanged="txtSendMail_TextChanged"></asp:TextBox>
        <strong>
            <asp:Label ID="Label4" runat="server" Text="Currency"></asp:Label></strong>
        <asp:DropDownList ID="currencySendType" CssClass="form-control" runat="server">
            <asp:ListItem>TR</asp:ListItem>
            <asp:ListItem>USD</asp:ListItem>
            <asp:ListItem>EU</asp:ListItem>
        </asp:DropDownList>
        <strong>
            <asp:Label ID="Label3" runat="server" Text="Amount"></asp:Label>
        </strong>
        <asp:TextBox ID="txtMoneyAmount" runat="server" CssClass="form-control" OnTextChanged="txtMoneyAmount_TextChanged"></asp:TextBox>
        <br />
        <asp:Button ID="sendMoneyButton" runat="server" CssClass="btn btn-block btn-success" OnClick="checkMail_Click" Text="Transmit Money" />
        <!-- <asp:Button ID="Button1" runat="server" OnClientClick = "Confirm()"  Text="Transmit Money" />-->
        <asp:Label ID="Transactionmesaj" runat="server" CssClass="label label-success" Text="Label" Visible="False"></asp:Label>
        <br />
       </div>
    </div>


    <script type="text/javascript">

        function Confirm() {
            if (Page_ClientValidate()) {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";

                if (confirm("Data has been Added. Do you wish to Continue ?") == true) {
                    confirm_value.value = "Yes";
                }
                else {
                    confirm_value.value = "No";
                }
                document.forms[0].appendChild(confirm_value);
            }
        }

    </script>
</asp:Content>
