﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMe
{
    public partial class SendAndRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Transactionmesaj.Text = " ";
        }
        protected void txtSendMail_TextChanged(object sender, EventArgs e)
        {


        }




        protected void txtMoneyAmount_TextChanged(object sender, EventArgs e)
        {

        }

        protected void checkMail_Click(object sender, EventArgs e)
        {
            DataTable dt, dt2;
            int transResult;
            double moneyConstrain = 0.0;






            dt = MailCheck(txtSendMail.Text);


            if (dt.Rows.Count > 0)
            {
                string UserID = (string)Session["UserID"];
                string receiverID = dt.Rows[0]["UserID"].ToString();

                string accountType = (string)Session["accountType"];





                if (accountType == "PERSONAL")
                {
                    moneyConstrain = 1000;

                }

                else if (accountType == "BUSINESS")
                {
                    moneyConstrain = 10000;

                }

                dt2 = moneyCheck(Int16.Parse(UserID), currencySendType.SelectedValue);
                double CustomerMoney = Convert.ToDouble(dt2.Rows[0]["Balance"]);
                double enteredMoney = Convert.ToDouble(txtMoneyAmount.Text);

                if (enteredMoney <= moneyConstrain)
                {
                    if (CustomerMoney >= enteredMoney)
                    {


                        
                        
                        Transactionmesaj.Visible = true;
                       
                        //float Balance, int TransactionType, int SenderID,int ReceiverID,int CurrencyID
                        transResult = sendMoney(float.Parse(txtMoneyAmount.Text), 2, Int16.Parse(UserID), Int16.Parse(receiverID), currencySendType.SelectedIndex + 1);
                        Transactionmesaj.Visible = true;

                        //if(enteredMoney > 100)
                        //{
                        //    double comision;
                        //    Response.Write("%2 komisyon alıncaktır");
                        //   comision = enteredMoney - (en) 
                        //}

                        if (transResult > 0)
                        {

                            Transactionmesaj.Text = "transaction done.";
                            //curText2.Text = dt2.Rows[0]["Balance"].ToString();
                        }
                        else
                        {

                            Transactionmesaj.Text = "transaction has not done.";
                        }

                    }

                    else
                    {

                        Transactionmesaj.Text = " not enough money  .";
                        // curText2.Text = dt2.Rows[0]["Balance"].ToString();
                    }
                }

                else
                {
                    Transactionmesaj.Text = "Your account is limited. Update your account to business account";

                }





            }
            else
            {
                Response.Write("<script>alert('Please enter valid mail ')</script>");

            }


        }

        public DataTable moneyCheck(int UserID, string currencyType)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.ShowBalanceByCurrencyType";
            SqlCommand com = new SqlCommand(query, con);


            com.Parameters.AddWithValue("@receiverID", UserID);
            com.Parameters.AddWithValue("@CurrencyType", currencyType);

            com.CommandType = CommandType.StoredProcedure;


            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;

        }

        /*
         
             
             
             
             
             */

        public DataTable MailCheck(string mail)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from [Info].[Users] where [Email]='" + mail + "' ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;

        }

        public int sendMoney(float Balance, int TransactionType, int SenderID, int ReceiverID, int CurrencyID)
        {
            int result;
            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.SendMoney";
            SqlCommand com = new SqlCommand(query, con);


            com.Parameters.AddWithValue("@Balance", Balance);
            com.Parameters.AddWithValue("@TransactionType", TransactionType);
            com.Parameters.AddWithValue("@SenderID", SenderID);
            com.Parameters.AddWithValue("@ReceiverID", ReceiverID);
            com.Parameters.AddWithValue("@CurrencyID", CurrencyID);

            com.CommandType = CommandType.StoredProcedure;

            result = com.ExecuteNonQuery();
            con.Close();


            return result;


        }


    }
}