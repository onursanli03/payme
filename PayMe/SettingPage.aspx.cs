﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMe
{
    public partial class SettingPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string UserID = (string)Session["UserID"];

                DataTable userTable = ShowUserInfo(Int16.Parse(UserID));
                txtSSN.Text = userTable.Rows[0]["SSN"].ToString();
                txtFirstName.Text = userTable.Rows[0]["FirstName"].ToString();
                txtLastName.Text = userTable.Rows[0]["LastName"].ToString();
                txtPassword.Text = userTable.Rows[0]["Password"].ToString();
                txtEmail.Text = userTable.Rows[0]["Email"].ToString();
                txtAdress.Text = userTable.Rows[0]["Address"].ToString();
                txtPhonenumber.Text = userTable.Rows[0]["PhoneNumber"].ToString();
                txtbirthday.Text = userTable.Rows[0]["BirthDate"].ToString();
                txtcountry.Text = userTable.Rows[0]["Country"].ToString();
                
                DataList1.DataSource = DisplayByImagePath(Int16.Parse(UserID));
                DataList1.DataBind();


            }


        }

        public DataTable DisplayByImagePath(int UserID)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;
            SqlConnection con2 = new SqlConnection(connectionString);

            string query2 = "Info.SelectUserImage";
            SqlCommand com2 = new SqlCommand(query2, con2);
            con2.Open();

            com2.Parameters.AddWithValue("@UserID", UserID);

            com2.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da2 = new SqlDataAdapter(com2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);

            return dt2;
        }

        protected void btnLogIn_Click(object sender, EventArgs e)
        {
            string UserID = (string)Session["UserID"];
            DateTime bdate = DateTime.ParseExact(birthDate.Text, "MM-dd-yyyy", CultureInfo.InvariantCulture);
            updateUserInfo(Int16.Parse(UserID), txtFirstName.Text, txtLastName.Text, Int32.Parse(txtSSN.Text), txtPassword.Text, txtEmail.Text, txtAdress.Text, Country.SelectedItem.Text, bdate, " ", txtPhonenumber.Text);

        }

        public DataTable ShowUserInfo(int UserID)
        {



            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);

            string query = "Info.ShowUserInfo";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);
            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);


            return dt;

        }

        public int updateUserInfo(int UserID, string FirstName, string LastName,
int SSN, string Password, string Email, string Address, string Country, DateTime BirthDate, string PicturePath, string PhoneNumber)
        {
            string fileName = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            FileUpload1.PostedFile.SaveAs(Server.MapPath(fileName));



            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);

            string query = "Info.UpdateUserProfile";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);
            com.Parameters.AddWithValue("@FirstName", FirstName);
            com.Parameters.AddWithValue("@LastName", LastName);
            com.Parameters.AddWithValue("@SSN", SSN);
            com.Parameters.AddWithValue("@Password", Password);
            com.Parameters.AddWithValue("@Email", Email);
            com.Parameters.AddWithValue("@Address", Address);
            com.Parameters.AddWithValue("@Country", Country);
            com.Parameters.AddWithValue("@BirthDate", BirthDate);
            com.Parameters.AddWithValue("@PicturePath", fileName);
            com.Parameters.AddWithValue("@PhoneNumber", PhoneNumber);


            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);

            con.Open();
            int res = com.ExecuteNonQuery();
            con.Close();

            return res;

        }

        
    }
}