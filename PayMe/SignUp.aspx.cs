﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace PayMe
{
    public partial class SignUp : System.Web.UI.Page
    {



        string conStr = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            ReadOnlyCollection<TimeZoneInfo> tz;
            tz = TimeZoneInfo.GetSystemTimeZones();
            for (int i = 0; i < tz.Count; i++)
            {
                drpTimeZone.Items.Add(tz[i].DisplayName);
            }


        }

        protected void btnSignUp_Click(object sender, EventArgs e)
        {
            DateTime date = DateTime.ParseExact(birthDate.Text, "MM-dd-yyyy", CultureInfo.InvariantCulture);
            int signresult;
            SqlConnection con = new SqlConnection(conStr);
            string query = "Info.RegisterUser";
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
            com.Parameters.AddWithValue("@LastName", txtLastName.Text);
            com.Parameters.AddWithValue("@SSN", Int32.Parse(txtSSN.Text));
            com.Parameters.AddWithValue("@Password", txtPassword.Text);
            com.Parameters.AddWithValue("@Email", txtEmail.Text);
            com.Parameters.AddWithValue("@Country", Country.SelectedItem.Text);
            com.Parameters.AddWithValue("@TimeZone", drpTimeZone.SelectedItem.Text);
            com.Parameters.AddWithValue("@Address", txtAddress.Text);
            com.Parameters.AddWithValue("@BirthDate", date);
            com.Parameters.AddWithValue("@SecurityQ", drpSq.SelectedItem.Text);
            com.Parameters.AddWithValue("@SecurityAns", txtSa.Text);

            com.CommandType = System.Data.CommandType.StoredProcedure;
            con.Open();
            signresult = com.ExecuteNonQuery();
            con.Close();

            if (signresult > 0)
            {

                DataTable dt4;
                dt4 = Login(txtEmail.Text, txtPassword.Text);
                int sadas = dt4.Rows.Count;
                if (dt4.Rows.Count > 0)
                {
                    string SignUserID = dt4.Rows[0]["UserID"].ToString();

                    newUserBalance(Int16.Parse(SignUserID));



                }

                
                  Response.Redirect("index.aspx");

            }
            else
            {
                Response.Write("<script>alert('not signed up.')</script>");
               

            }

        }

        public DataTable Login(string mail, string password)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from [Info].[Users] where ([Email]='" + mail + "') AND [Password]='" + password + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;

        }

        public int newUserBalance(int userID)
        {
            int signresult2;
            SqlConnection con2 = new SqlConnection(conStr);
            string query2 = "Info.newUserBalance";
            SqlCommand com2 = new SqlCommand(query2, con2);
            com2.Parameters.AddWithValue("@UserID", userID);

            com2.CommandType = System.Data.CommandType.StoredProcedure;
            con2.Open();
            signresult2 = com2.ExecuteNonQuery();
            con2.Close();

            return signresult2;

        }
    }
}