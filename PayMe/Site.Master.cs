﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMe
{
    public partial class SiteMaster : MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {

                profileImage.Src = "image/" + (string)Session["PicturePath"];
                lblName.Text = "Welcome : " + (string)Session["FirstName"] + " " + (string)Session["LastName"];
            }
            else
            {

                Response.Redirect("Login.aspx");
            }

        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LogIn.aspx");
        }
    }
}

