﻿<%@ Page Title="Summary" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Summary.aspx.cs" Inherits="PayMe.Summary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="col-md-8 col-sm-offset-4">
        <div class="btn-group">
            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-lg btn-success" OnClick="Button2_Click"><i class="glyphicon glyphicon-credit-card"></i> Add Card</asp:LinkButton>
            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-lg btn-warning" OnClick="LinkButton2_Click"><i class="glyphicon glyphicon-piggy-bank"></i> Bank Information</asp:LinkButton>
        </div>
    </div>

    <br />
    <br />
    <br />
    <br />


    <div class="col-md-12 ">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4 style="text-align: center"><i class="glyphicon glyphicon-credit-card"></i>Balance</h4>
            </div>

            <asp:DataList ID="currencyList" runat="server">
                <ItemTemplate>
                    <strong>
                        <asp:Label ID="Label1" CssClass="" runat="server" Text='<%# Eval("Balance") %>'></asp:Label>
                        <asp:Label ID="Label2"  runat="server" Text='<%# Eval("CurrencyType") %>'></asp:Label>
                    </strong>
                </ItemTemplate>
            </asp:DataList>

            <strong>
                <asp:Label ID="totalText" runat="server" Text="total : "></asp:Label></strong>
            <strong>
                <asp:Label ID="totalCurText" runat="server" Text="Label"></asp:Label></strong>
             <strong>
                <asp:Label ID="totalType" runat="server" Text="TR"></asp:Label></strong>


        </div>
    </div>




    <br />

    <div class="row">

        <div class="col-md-8 col-md-offset-1">
            <h3 style="text-align: center">Your Cards</h3>
            <br />
            <div class="col-md-6 col-md-offset-3">
                <asp:Label ID="nocard" runat="server" Text="No Card" Visible="False"></asp:Label>
                <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary btn-block block" OnClick="Button1_Click" Text="Edit Card" />
            </div>

        </div>
        <br />
        <div class="col-md-8 col-md-offset-3">
            <asp:DataList ID="cardList" runat="server">
                <ItemTemplate>
                    <strong>
                        <table style="width: 100%;" class="table table-respponsive">
                            <tr>

                                <td>
                                    <strong>
                                        <asp:Label ID="info1" runat="server" Text="Card Number"></asp:Label>
                                    </strong>
                                </td>
                                <td><strong>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("cardNumber") %>'></asp:Label>
                                </strong></td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <asp:Label ID="info2" runat="server" Text="Expire Date"></asp:Label>
                                    </strong>
                                </td>
                                <td><strong>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("expireDate") %>'></asp:Label>
                                </strong></td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <asp:Label ID="info3" runat="server" Text="CVV"></asp:Label>
                                    </strong>
                                </td>
                                <td><strong>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("cvv") %>'></asp:Label>
                                </strong></td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        <asp:Label ID="info4" runat="server" Text="Take for Edit"></asp:Label></strong>
                                </td>
                                <td>

                                    <input id="card" type="radio" name="cardGroup" value='<%#Eval("CardInfoID") %>' />
                                </td>
                            </tr>


                        </table>
                    </strong>
                </ItemTemplate>
            </asp:DataList>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <h3 style="text-align: center">Your Bank Accounts</h3>
            <br />
            <div class="col-md-6 col-md-offset-3">
                <asp:Label ID="bankAccount" runat="server" Text="No Bank Account" Visible="False"></asp:Label>
                <asp:Button ID="Button2" runat="server" CssClass="btn btn-success btn-block" OnClick="EditAccountbtn_Click" Text="Edit Account" />
            </div>
        </div>
        <div class="col-md-8 col-md-offset-3">
            <asp:DataList ID="bankInfoList" runat="server">
                <ItemTemplate>
                    <strong>
                        <table style="width: 100%;" class="table table-responsive">
                            <tr>
                                <strong>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text="Bank Name"></asp:Label></td>
                                </strong>
                                <td><strong>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("BankName") %>'></asp:Label>
                                </strong></td>
                            </tr>
                            <tr>
                                <strong>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Text="Account Number"></asp:Label></td>
                                </strong>
                                <td><strong>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("AccountNumber") %>'></asp:Label>
                                </strong></td>
                            </tr>
                            <tr>
                                <strong>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Take for Edit"></asp:Label></td>
                                </strong>
                                <td>
                                    <input id="bank" type="radio" name="bankGroup" value='<%#Eval("BankAccountInfoID") %>' />
                                </td>
                            </tr>
                        </table>
                    </strong>
                </ItemTemplate>
            </asp:DataList>
        </div>
    </div>
    <br />


</asp:Content>
