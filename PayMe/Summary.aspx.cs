﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace PayMe
{
    public partial class Summary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string UserID = (string)Session["UserID"];
            // UserID = "1";
            DataTable datable;
            DataTable datable2;
            DataTable cardTable, bankTable;
            datable = ShowBalanceByUserID(int.Parse(UserID));
            cardTable = DisplayCardInfo(int.Parse(UserID));
            bankTable = DisplayBankInfo(int.Parse(UserID));


            if (datable.Rows.Count > 0)
            {

                //showCurreny1.Text = datable.Rows[0]["Balance"].ToString();
                /*currencyService.CurrencyConvertorSoapClient ioclient = new currencyService.CurrencyConvertorSoapClient();
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(ioclient.ConversionRate(currencyService.Currency.USD, currencyService.Currency.TRY).ToString());

                cur = curTextTrial.Text;*/
                datable2 = source();
                decimal usd = Convert.ToDecimal(datable2.Rows[0][2]) * Convert.ToDecimal(datable.Rows[1]["Balance"]) / 10000;
                decimal eur = Convert.ToDecimal(datable2.Rows[3][2]) * Convert.ToDecimal(datable.Rows[2]["Balance"]) / 10000;
                totalCurText.Text = (eur + usd + Convert.ToDecimal(datable.Rows[0]["Balance"])).ToString();
               


                currencyList.DataSource = datable;
                currencyList.DataBind();
            }
            int cardcount = cardTable.Rows.Count;
            int bankcount = bankTable.Rows.Count;
            if (cardcount > 0)
            {
                cardList.DataSource = cardTable;
                cardList.DataBind();

            }
            else
            {
                nocard.Visible = true;
                nocard.Text = "No card";

            }

            if (bankcount > 0)
            {
                bankInfoList.DataSource = bankTable;
                bankInfoList.DataBind();

            }
            else
            {
                bankAccount.Visible = true;
                bankAccount.Text = "No Bank Account";

            }
        }
        public DataTable ShowBalanceByUserID(int UserID)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.ShowBalanceByUserID";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@receiverID", UserID);
            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;


        }
        public DataTable source()
        {
            DataTable dt = new DataTable();
            // DataTable nesnemizi yaratıyoruz
            DataRow dr;
            // DataTable ın satırlarını tanımlıyoruz.
            dt.Columns.Add(new DataColumn("Adı", typeof(string)));
            dt.Columns.Add(new DataColumn("Kod", typeof(string)));

            dt.Columns.Add(new DataColumn("Efektif Satış", typeof(string)));
            // DataTableımıza 6 sütün ekliyoruz ve değişken tiplerini tanımlıyoruz.
            XmlTextReader rdr = new XmlTextReader("http://www.tcmb.gov.tr/kurlar/today.xml");
            // XmlTextReader nesnesini yaratıyoruz ve parametre olarak xml dokümanın urlsini veriyoruz
            // XmlTextReader urlsi belirtilen xml dokümanlarına hızlı ve forward-only giriş imkanı sağlar.
            XmlDocument myxml = new XmlDocument();
            // XmlDocument nesnesini yaratıyoruz.
            myxml.Load(rdr);
            // Load metodu ile xml yüklüyoruz
            XmlNode tarih = myxml.SelectSingleNode("/Tarih_Date/@Tarih");
            XmlNodeList mylist = myxml.SelectNodes("/Tarih_Date/Currency");
            XmlNodeList adi = myxml.SelectNodes("/Tarih_Date/Currency/Isim");
            XmlNodeList kod = myxml.SelectNodes("/Tarih_Date/Currency/@Kod");
            XmlNodeList efektif_satis = myxml.SelectNodes("/Tarih_Date/Currency/BanknoteSelling");
            // XmlNodeList cinsinden her bir nodu, SelectSingleNode metoduna nodların xpathini parametre olarak
            // göndererek tanımlıyoruz.

            //GridView1.Caption = tarih.InnerText.ToString() + " tarihli merkez bankası kur bilgileri";

            // datagridimin captionu ayarlıyoruz.

            /*  Burada xmlde bahsettiğim - bence-  mantık hatasından dolayı x gibi bir değişken tanımladım.
            bu x =19  DataTable a sadece 19 satır eklenmesini sağlıyor. çünkü xml dökümanında 19. node dan sonra
            güncel kur bilgileri değil Euro dönüşüm kurları var ve bu node dan sonra yapı ilk 18 node ile tutmuyor
            Bence ayrı bir xml dökümanda tutulması gerekirdi. 
            */
            int x = 4;
            for (int i = 0; i < x; i++)
            {
                dr = dt.NewRow();
                dr[0] = adi.Item(i).InnerText.ToString(); // i. adi nodunun içeriği
                                                          // Adı isimli DataColumn un satırlarını  /Tarih_Date/Currency/Isim node ları ile dolduruyoruz.
                dr[1] = kod.Item(i).InnerText.ToString();
                // Kod satırları

                dr[2] = efektif_satis.Item(i).InnerText.ToString();
                // Efektif Satış.
                dt.Rows.Add(dr);
            }
            // DataTable ımızın satırlarını 18 satır olacak şekilde dolduruyoruz
            // gerçi yine web mastırın insafı devreye giriyor:).
            // yukarıda bahsettiğim sorun.
            return dt;
            // DataTable ı döndürüyoruz.
        }

        public DataTable addCardInformation(int UserID, string BankName, string AccountNumber, int bin, int cardNumber, DateTime expireDate, int cvv)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.SaveBankInformation";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);
            com.Parameters.AddWithValue("@BankName", BankName);
            com.Parameters.AddWithValue("@AccountNumber", AccountNumber);
            com.Parameters.AddWithValue("@bin", bin);
            com.Parameters.AddWithValue("@cardNumber", cardNumber);
            com.Parameters.AddWithValue("@expireDate", expireDate);
            com.Parameters.AddWithValue("@cvv", cvv);
            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;


        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddCard");
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddBankInformation");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            if (Request.Form["cardGroup"] != null)

            {

                Session["cardInfo"] = Request.Form["cardGroup"];
                Response.Redirect("cardInfoEdit.aspx");

            }


        }

        public DataTable DisplayCardInfo(int UserID)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.DisplayCardInfo";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);


            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;


        }


        public DataTable DisplayBankInfo(int UserID)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.DisplayBankInfo";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);


            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;


        }

        protected void EditAccountbtn_Click(object sender, EventArgs e)
        {


            if (Request.Form["bankGroup"] != null)

            {

                Session["bankInfo"] = Request.Form["bankGroup"];
                Response.Redirect("bankAccountInfoEdit.aspx");

            }

        }
    }




}
