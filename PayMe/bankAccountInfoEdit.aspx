﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="bankAccountInfoEdit.aspx.cs" Inherits="PayMe.bankAccountInfoEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">

        <div class="col-md-12">
            <div class="col-md-10">
                <h3>Edit Bank Account</h3>
                <hr />
            </div>
            <div class="col-md-10">
                <strong><asp:Label ID="info1" runat="server" Text=" Bank Name : "></asp:Label></strong>
                <asp:TextBox ID="BankName" CssClass="form-control" runat="server" placeholder="BankName" ReadOnly="True"></asp:TextBox>
                <br />
                <strong><asp:Label ID="info2" runat="server" Text="Bank : "></asp:Label></strong>
                <asp:DropDownList CssClass="form-control" ID="bankList" runat="server">
                    <asp:ListItem Value="AB">Akbank</asp:ListItem>
                    <asp:ListItem Value="IB">İş Bankası</asp:ListItem>
                    <asp:ListItem Value="ZB">Ziraat Bankası</asp:ListItem>
                    <asp:ListItem Value="GB">Garanti Bankası</asp:ListItem>
                    <asp:ListItem Value="TEB">Türkiye Ekonomi Bankası</asp:ListItem>
                    <asp:ListItem Value="DB">Deniz Bank</asp:ListItem>
                </asp:DropDownList>
                <br />
                <strong><asp:Label ID="info3" runat="server" Text="Account Number : "></asp:Label></strong>
                <asp:TextBox ID="AccountNumber" CssClass="form-control" runat="server" placeholder="AccountNumber"></asp:TextBox>

                <br />
                <asp:Label ID="processInfo" CssClass="label label-success" runat="server" Text="Label" Visible="False"></asp:Label>
                <br />
                <div class="form form-inline">
                    <asp:Button ID="updatebnkbtn" CssClass="btn btn-primary" runat="server" OnClick="updatebtn_Click" Text="Update" />
                    <asp:Button ID="deletebnkbtn" CssClass="btn btn-danger" runat="server" OnClick="deletebtn_Click" Text="Delete" />


                </div>

            </div>
        </div>
    </div>



</asp:Content>
