﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMe
{
    public partial class bankAccountInfoEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string UserID = (string)Session["UserID"];
                string bankInfo = (string)Session["bankInfo"];
                processInfo.Visible = false;

                DataTable displayAccount = DisplayBankInfoBybid(int.Parse(UserID), Int16.Parse(bankInfo));

                BankName.Text = displayAccount.Rows[0]["BankName"].ToString();
                AccountNumber.Text = displayAccount.Rows[0]["AccountNumber"].ToString();

            }
        }

        protected void updatebtn_Click(object sender, EventArgs e)
        {
            string UserID = (string)Session["UserID"];
            string bankInfo = (string)Session["bankInfo"];
            int updateResult = UpdateBankAccountInfo(int.Parse(UserID), Int16.Parse(bankInfo), bankList.SelectedItem.Text, AccountNumber.Text);
            processInfo.Visible = true;

            if (updateResult > 0)
            {
                processInfo.Text = "Card Updated";


            }
            else
            {
                processInfo.Text = "Card could not be updated";

            }
        }

        protected void deletebtn_Click(object sender, EventArgs e)
        {
            string UserID = (string)Session["UserID"];
            string bankInfo = (string)Session["bankInfo"];
            int deleteResult = DeleteBankAccountInfo(int.Parse(UserID), Int16.Parse(bankInfo));
            processInfo.Visible = true;

            if (deleteResult > 0)
            {
                processInfo.Text = "Account could not be deleted";

            }
            else
            {

                Response.Redirect("Summary.aspx");

            }

        }


        public DataTable DisplayBankInfoBybid(int UserID, int BankAccountInfoID)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.DisplayBankInfoByBib";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);
            com.Parameters.AddWithValue("@BankAccountInfoID", BankAccountInfoID);


            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;


        }

        public int UpdateBankAccountInfo(int UserID, int BankAccountInfoID, string BankName, string AccountNumber)
        {



            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con2 = new SqlConnection(connectionString);

            string query2 = "Info.UpdateBankAccountInfo";
            SqlCommand com2 = new SqlCommand(query2, con2);

            com2.Parameters.AddWithValue("@UserID", UserID);
            com2.Parameters.AddWithValue("@BankAccountInfoID", BankAccountInfoID);
            com2.Parameters.AddWithValue("@BankName", BankName);
            com2.Parameters.AddWithValue("@AccountNumber", AccountNumber);


            com2.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da2 = new SqlDataAdapter(com2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);

            con2.Open();
            int res2 = com2.ExecuteNonQuery();
            con2.Close();

            return res2;

        }


        public int DeleteBankAccountInfo(int UserID, int BankAccountInfoID)
        {



            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);

            string query = "Info.DeleteBankAccountInfo";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);
            com.Parameters.AddWithValue("@BankAccountInfoID", BankAccountInfoID);

            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);

            con.Open();
            int res = com.ExecuteNonQuery();
            con.Close();

            return res;

        }

    }
}