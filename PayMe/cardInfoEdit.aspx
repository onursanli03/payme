﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cardInfoEdit.aspx.cs" Inherits="PayMe.cardInfoEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container">
        <div class="col-md-12">
            <div class="col-md-10">
                <h3>Edit Card Info</h3>
                <hr />
            </div>
            <div class="col-md-10">
                
                    <strong><asp:Label ID="info1" runat="server" Text="Card Number : "></asp:Label></strong>
                    <asp:TextBox ID="txtCardNumber" CssClass="form-control" runat="server" placeholder="Card Number"></asp:TextBox>
                
                <br />
                
                     <strong><asp:Label ID="info2" runat="server" Text="Expire Date : "></asp:Label></strong>
                    <asp:TextBox ID="txtExpireDate" CssClass="form-control" runat="server" placeholder="Expire Date"></asp:TextBox>
                
                <br />
                
                     <strong><asp:Label ID="info3" runat="server" Text="CVV : "></asp:Label></strong>
                    <asp:TextBox ID="txtCVV" CssClass="form-control" runat="server" placeholder="CVV"></asp:TextBox>
                
                <asp:Label ID="processInfo" runat="server" Text="Label" Visible="False"></asp:Label>
                <br />
                <div class="form form-inline">
                    <asp:Button ID="updatebtn" runat="server" CssClass="btn btn-primary" OnClick="updatebtn_Click" Text="Update" />
                    <asp:Button ID="deletebtn" runat="server" CssClass="btn btn-danger" OnClick="deletebtn_Click" Text="Delete" />
                </div>
            </div>
        </div>
    </div>



</asp:Content>
