﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayMe
{
    public partial class cardInfoEdit : System.Web.UI.Page
    {



        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string UserID = (string)Session["UserID"];
                string cardInfo = (string)Session["cardInfo"];
                processInfo.Visible = false;

                DataTable displayCard = DisplayCardInfoByCid(int.Parse(UserID), Int16.Parse(cardInfo));

                txtCardNumber.Text = displayCard.Rows[0]["cardNumber"].ToString();
                txtExpireDate.Text = displayCard.Rows[0]["expireDate"].ToString();
                txtCVV.Text = displayCard.Rows[0]["cvv"].ToString();
            }
        }

        protected void updatebtn_Click(object sender, EventArgs e)
        {
            string UserID = (string)Session["UserID"];
            string cardInfo = (string)Session["cardInfo"];
            int updateResult = updateCardInfo(int.Parse(UserID), Int16.Parse(cardInfo), txtCardNumber.Text, txtExpireDate.Text, Int16.Parse(txtCVV.Text));
            processInfo.Visible = true;

            if (updateResult > 0)
            {
                processInfo.Text = "Card Updated";


            }
            else
            {
                processInfo.Text = "Card could not be updated";

            }

        }

        protected void deletebtn_Click(object sender, EventArgs e)
        {

            string UserID = (string)Session["UserID"];
            string cardInfo = (string)Session["cardInfo"];
            int deleteResult = deleteCardInfo(int.Parse(UserID), Int16.Parse(cardInfo));
            processInfo.Visible = true;

            if (deleteResult > 0)
            {
                processInfo.Text = "Card could not be deleted";

            }
            else
            {

                Response.Redirect("Summary.aspx");

            }

        }

        public DataTable DisplayCardInfoByCid(int UserID, int CardInfoID)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            string query = "Info.DisplayCardInfoByCid";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);
            com.Parameters.AddWithValue("@CardInfoID", CardInfoID);


            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;


        }

        public int updateCardInfo(int UserID, int CardInfoID, string cardNumber, string expireDate, int cvv)
        {



            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con2 = new SqlConnection(connectionString);

            string query2 = "Info.UpdateCardInfo";
            SqlCommand com2 = new SqlCommand(query2, con2);

            com2.Parameters.AddWithValue("@UserID", UserID);
            com2.Parameters.AddWithValue("@CardInfoID", CardInfoID);
            com2.Parameters.AddWithValue("@cardNumber", cardNumber);
            com2.Parameters.AddWithValue("@expireDate", expireDate);
            com2.Parameters.AddWithValue("@cvv", cvv);



            com2.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da2 = new SqlDataAdapter(com2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);

            con2.Open();
            int res2 = com2.ExecuteNonQuery();
            con2.Close();

            return res2;

        }


        public int deleteCardInfo(int UserID, int CardInfoID)
        {



            string connectionString = ConfigurationManager.ConnectionStrings["PameDb"].ConnectionString;

            SqlConnection con = new SqlConnection(connectionString);

            string query = "Info.DeleteCardInfo";
            SqlCommand com = new SqlCommand(query, con);

            com.Parameters.AddWithValue("@UserID", UserID);
            com.Parameters.AddWithValue("@CardInfoID", CardInfoID);




            com.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);

            con.Open();
            int res = com.ExecuteNonQuery();
            con.Close();

            return res;

        }

    }
}