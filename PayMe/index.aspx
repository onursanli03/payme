﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="PayMe.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pay Me</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700,600' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="Content/base.css" />
    <link rel="stylesheet" href="Content/skeleton.css" />
    <link rel="stylesheet" href="Content/layout.css" />
    <link rel="stylesheet" href="Content/layout.css" />
    <link rel="stylesheet" href="Content/responsivemobilemenu.css" />
    <link rel="stylesheet" href="Content/flexslider.css" />
    <link rel="stylesheet" href="Content/prettyPhoto.css" />
   

    <!-- JS
  ================================================== -->

    <script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="Scripts/responsivemobilemenu.js"></script>
    <script type="text/javascript" src="Scripts/jquery.flexslider.js"></script>
    <script type="text/javascript" src="Scripts/jquery.testemonialslider.js"></script>
    <script type="text/javascript" src="Scripts/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="Scripts/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="Scripts/jquery.quicksand.js"></script>
    <script type="text/javascript" src="Scripts/script.js"></script>
    <script src="Scripts/script.js"></script>
    <!-- SMOOTH SCROLLING -->
    <script type="text/javascript" src="javascripts/smoothscroll.js"></script>


    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

    <!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="image/Logo_payme_2_50.png" />
    <link rel="apple-touch-icon" href="image/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="image/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="image/apple-touch-icon-114x114.png" />

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-46811000-1', 'fabianbentz.de');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="top">
    <a href="#home"><img id="rotate" src="images/top.png" width="70" height="70" alt="top" /></a>
</div>

<header>
<div class="container nav" id="home">
      <div class="three columns logo center">
      <img src="image/Logo_payme_2_500.png" height="120px" width="120px" alt"" />
      </div>
      <div class="thirteen columns navigation">
        <nav>
          <div class='rmm' data-menu-style = "minimal">
            <ul>
                <li><a href='#home'>HOME</a></li>
                <li><a href='#service'>SERVICE</a></li>
                <li><a href='#team'>TEAM</a></li>
                <li><a href='#contact'>CONTACT</a></li>
                <li><a href='InvoiceControl.aspx'>Check Invoice</a></li> 
                <li><a href="login.aspx">Log in</a></li>
                <li><a href="signup.aspx">Sign Up</a></li>
            </ul>
          </div>
        </nav>
      </div>
     </div>

</header>

<div class="service" id="service">
    <div class="container">
    
        <div class="sixteen columns">
        
         <h2 class="white">Our Service</h2>
         <hr class="white">
         
         <div class="four columns alpha">
         <img class="center circle scale-with-grid" id="rotate" src="image/Icon_payMe-01.svg"/>
         <h4 class="white">Transfer easily</h4>
         <p class="center white">Customers can transfer easily their money to friends, family members etc </p>
         
         </div>
         
         <div class="four columns alpha">
         <img class="center circle scale-with-grid" id="rotate" src="image/Icon_payMe-02.svg"/>
         <h4 class="white">Saving Future</h4>
         <p class="center white">Customers have chance to save your their future in digital world</p>
        
         </div>
         
         <div class="four columns alpha">
         <img class="center circle scale-with-grid" id="rotate" src="image/Icon_payMe-03.svg">
         <h4 class="white">Gaming World</h4>
         <p class="center white">Customers have many opportunities and discount in game stores like Steam</p>
        
         </div>
         
         <div class="four columns alpha omega">
         <img class="center circle scale-with-grid" id="rotate" src="image/Icon_payMe-04.svg">
         <h4 class="white">Trustable Safe</h4>
         <p class="center white">Our company insures safety to customer's futures, savings and many investments</p>
         
         </div>
        
        </div>
        
    </div>
</div>



<div class="team" id="team">
    <div class="container">
    
        <div class="sixteen columns">
        
         <h2>Meet Our Team</h2>
         <hr/>
         
         
         <div class="eight columns alpha member">
         <img id="zoom" class="center scale-with-grid" src="image/15401006_10211258894851832_1233361030848338866_n.jpg" />
         <h4>Onur Şanlı</h4>
         <h6>Founder / CEO</h6>
         
         <p  class="center"> <a class="social" href="https://www.facebook.com/onursanli03" alt=""><img class="scale-with-grid" src="./images/icons/social/facebook.svg" alt=""></a>
         <a class="social" href="https://plus.google.com/+Onur%C5%9Eanl%C4%B1" alt=""><img class="scale-with-grid" src="./images/icons/social/google.svg" alt=""></a>
         <a class="social" href="https://twitter.com/Onursanli03" alt=""><img class="scale-with-grid" src="./images/icons/social/twitter.svg" alt=""></a>
         <a class="social" href="https://www.linkedin.com/in/onur-%C5%9Fanl%C4%B1-18363382?trk=nav_responsive_tab_profile" alt=""><img class="scale-with-grid" src="./images/icons/social/linkedin.svg" alt=""></a>
         </p>
         </div>
         
         <div class="eight columns alpha member">
         <img id="zoom" class="center scale-with-grid" src="image/^4A1D73FFC0FEF5E24D2DCB0E73596E68BB89DD6720D9B5F6DF^pimgpsh_fullsize_distr.jpg" />
         <h4>Masum Celil Olgun</h4>
         <h6>Founder / CEO</h6>
        
         <p class="center"><a class="social" href="https://www.facebook.com/Yepinnocent?fref=ts" alt=""><img class="scale-with-grid" src="./images/icons/social/facebook.svg" alt=""></a>
         <a class="social" href="https://twitter.com/hayaletyazars?lang=tr" alt=""><img class="scale-with-grid" src="./images/icons/social/twitter.svg" alt=""></a>
         </p>
         </div>
         
         </div>
        
    </div>
</div>


<!-- GOOGLE MAP -->

<div id="contact">
    <iframe class="border" width="100%" height="400"  src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3011.550639021566!2d28.8321859!3d40.9913196!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14caa35ee1fdc1b1%3A0x658a212e6b4359b5!2zxLBzdGFuYnVsIEvDvGx0w7xyIMOcbml2ZXJzaXRlc2k!5e0!3m2!1str!2str!4v1482281436834"  frameborder="0" scrolling="no" marginheight="0" marginwidth="0" alt""></iframe>
</div>

<!-- GOOGLE MAP END -->

<div id="contactform">

<div class="container">

   <div class="sixteen columns">

         <h2 class="white" >Contact Us</h2>
         <hr class="white" />
         <p class="center white">Our policies depends on your demands. If you want to give feedback or have problem in our service, you can easily communicate with us.</p>
    
    </div>
         
       <div class="two-thirds column form">
                 <div action="#" id="commentForm" method="post" name="commentForm">
                     <asp:TextBox  CssClass="required name" runat="server" id="txtName" name="name" type="text" value="Name*" onFocus="if(this.value=='Name*') this.value=''"></asp:TextBox>
                     <asp:TextBox  CssClass="required email" runat="server" id="txtEmail" name="email" type="text" value="Email*" onFocus="if(this.value=='Email*') this.value=''"></asp:TextBox>
                     <asp:TextBox  CssClass="required subject" runat="server" id="txtSubject" name="subject" type="text" value="Subject*" onFocus="if(this.value=='Subject*') this.value=''"></asp:TextBox>
                     <asp:TextBox  CssClass="required message" ID="txtBody" runat="server" TextMode="MultiLine" Rows="20" Width="588px" onFocus="if(this.value=='Message*') this.value=''">Message*</asp:TextBox>
                     <asp:Button runat="server" CssClass="btn btn-sm" text="Send" onclick="Button1_Click"/>
                       <p class="white">Required fields are marked *</p>
                  </div>
           <p>
                <asp:Label ID="DisplayMessage" runat="server" Visible="false" />
           </p> 
       </div>
       
       <div class="one-third column omega coninfo">
         <h4 class="left white">Contact Informations</h4>
         <hr class="white" />
         
         <ul>
          
          <li>Kültür University</li>
          <li>34000 İstanbul</li>
          <li>0212 444 4 444</li>
          <li><a href="mailto:">info@payme.com</a></li>
         </ul>
         <h4 class="left white">Follow Us</h4>
         <a class="social" href="" alt=""><img class="scale-with-grid" src="./images/icons/social/facebook.svg" alt=""></a>
         <a class="social" href="" alt=""><img class="scale-with-grid" src="./images/icons/social/google.svg" alt=""></a>
         <a class="social" href="" alt=""><img class="scale-with-grid" src="./images/icons/social/twitter.svg" alt=""></a>
         <a class="social" href="" alt=""><img class="scale-with-grid" src="./images/icons/social/behance.svg" alt=""></a>
         <a class="social" href="" alt=""><img class="scale-with-grid" src="./images/icons/social/linkedin.svg" alt=""></a>
         <a class="social" href="" alt=""><img class="scale-with-grid" src="./images/icons/social/dribbble.svg" alt=""></a>
         </div>
   
   </div>
</div>



<footer>
  <div class="container">
      <div class="sixteen columns copyright">
          
          <p class="white">Copyright <%=DateTime.Now.Year%> &copy PayMe Team all rights reserved</p>
          
      </div>
  </div>
</footer>

         


	

<!-- End Document
================================================== -->

<script type="text/javascript">
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide"
        });
    });
</script>



<!-- pretty photo 
	<script>
		$(document).ready(function(){
			$("a[class^='prettyPhoto']").prettyPhoto({
				social_tools: false,
				theme: 'light_square'
			});
		});
	</script>
	-->
	
	<script type="text/javascript" charset="utf-8">
	    $(document).ready(function () {
	        $("a[rel^='prettyPhoto']").prettyPhoto({
	            social_tools: false,
	            theme: 'light_square'
	        });
	    });
	</script>
    </form>
</body>
</html>
